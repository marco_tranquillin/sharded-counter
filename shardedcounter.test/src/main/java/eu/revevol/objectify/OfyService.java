package eu.revevol.objectify;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

import eu.revevol.shardedcounter.entity.Counter;
import eu.revevol.shardedcounter.entity.Shard;
import eu.revevol.shardedcounter.entity.TxnKey;

public class OfyService {
	static {
		ObjectifyService.register(Counter.class);
	    ObjectifyService.register(Shard.class);
	    ObjectifyService.register(TxnKey.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}

