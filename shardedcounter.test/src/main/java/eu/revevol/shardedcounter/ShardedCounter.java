package eu.revevol.shardedcounter;

import static eu.revevol.objectify.OfyService.ofy;

import java.util.Random;

import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.memcache.MemcacheService.SetPolicy;
import com.googlecode.objectify.VoidWork;

import eu.revevol.shardedcounter.entity.Counter;
import eu.revevol.shardedcounter.entity.Shard;
import eu.revevol.shardedcounter.entity.TxnKey;

/*
 * Class that describes a sharded counter that is capable of incrementing 
 * the counter and increasing the number of shards. 
 * Shard to be incremented is selected randomly a run-time
 * to prevent concurrent modifications.
 */

public class ShardedCounter {

	//constants
	private final int CACHE_EXPIRATION_TIME=60;
	private final Random RND=new Random();
	private final MemcacheService MC = MemcacheServiceFactory.getMemcacheService();
	
	//variables
	String name;
	Counter counter;
	
	/**
	 * Default constructor of the sharded counter 
	 */
	public ShardedCounter(){
		;
	}
	
	/**
	 * Load the counter
	 * @param counterName
	 * @return
	 */
	public boolean load(String counterName){
		//check for a Counter in the datastore
		this.counter=ofy().load().type(Counter.class).id(counterName).now();
		if(counter==null)
			return false; //this counter is not present
		return true;
	}
	
	/**
	 * Initialize the ShardedCounter
	 * @param name the name of the Sharded Counter
	 * @param numberOfShards the number of the shards of the counter
	 * @return true if the ShardedCounter has been correctly initialize, false otherwise
	 */
	public boolean initialize(String name, int numberOfShards){
		//check for a Counter in the datastore
		this.counter=ofy().load().type(Counter.class).id(name).now();
		if(counter!=null)
			return false; //this counter is already in use
		
		//save counter
		counter=new Counter();
		counter.setName(name);
		counter.setNumberOfShards(numberOfShards);
		ofy().save().entity(counter).now();
		//create value in the memcache
		MC.put(counter.getName(), new Long(0),Expiration.byDeltaSeconds(CACHE_EXPIRATION_TIME),SetPolicy.ADD_ONLY_IF_NOT_PRESENT);
		
		return true;
	}
	
	/**
	 * Get the sharded counter number of shards
	 * @return the number of shards
	 */
	public int getNumberOfShards(){
		return this.counter.getNumberOfShards();
	}
	
	/**
	 * Update the value of a shard. 
	 * To guarante the idempotence a UUID will be used a transaction key.
	 * @param txnKey the UUID value that identify a single transaction
	 * @param increment true if you want to increment the value of a shard, false otherwise
	 */
	public void updateShard(final String txnKeyUUID,final boolean increment){
		
		/*
		 * Update of the shard will be performed in a transaction.
		 * As reported in [1] an exectpion could be raisen also in case of completed transaction.
		 * In order to assure idempotence it will use a txnKey (UUID)
		 * 
		 * [1] https://cloud.google.com/appengine/docs/java/datastore/transactions
		 * 
		 * A transaction is a set of Datastore operations on one or more entities. 
		 * Each transaction is guaranteed to be atomic, which means that transactions are never partially applied. 
		 * Either all of the operations in the transaction are applied, or none of them are applied. 
		 * [...]
		 * Note: If your application receives an exception when committing a transaction, 
		 * it does not always mean that the transaction failed. 
		 * You can receive DatastoreTimeoutException or DatastoreFailureException exceptions 
		 * in cases where transactions have been committed and eventually will be applied successfully. 
		 * Whenever possible, make your Datastore transactions idempotent so that if you repeat a transaction, 
		 * the end result will be the same.
		 * [...]
		 */
		
		ofy().transact(new VoidWork(){
			@Override
			public void vrun() {
				//search for the txnKey
				TxnKey txnKey=ofy().load().type(TxnKey.class).id(txnKeyUUID).now();
				//only if the transaction key is not present go ahead
				if(txnKey==null){
					//randomly select a shard
					int shardNum = RND.nextInt(counter.getNumberOfShards());
					//update the shard
					Shard shard=ofy().load().type(Shard.class).id(counter.getName()+"_"+shardNum).now();
					if(shard==null){
						//create new shard
						shard=new Shard();
						shard.setName(counter.getName()+"_"+shardNum);
					}
					if(increment){ //increment/decrement shard value
						shard.increment();
					}
					else{
						shard.decrement();
					}
					//update shard value
					ofy().save().entity(shard).now();
					//save transaction key
					txnKey=new TxnKey(txnKeyUUID);
					ofy().save().entity(txnKey).now();
				}
			}
		});
		
		//update memCache
		if(increment){
			MC.increment(counter.getName(), 1);
		}
		else{
			MC.increment(counter.getName(), -1);
		}
		
		//remove TxnKey
		ofy().delete().type(TxnKey.class).id(txnKeyUUID).now();
	}
	
	/**
	 * Reset counter
	 */
	public void resetCounter(){
		//remove all shards
		for(int i=0;i<this.getNumberOfShards();i++){
			ofy().delete().type(Shard.class).id(this.counter.getName()+"_"+i).now();
		}
		//reset counter on memcache
		MC.put(this.counter.getName(), new Long(0));
		
	}
	
	/**
	 * Return the value of the sharded counter
	 * @return value of the sharded counter
	 */
	public long getCounterValue(){
		//try to get data from the memcache
		Long value=(Long)MC.get(counter.getName());
		if(value!=null)
			return value;
		
		//sum values of all shards
		long sum=0;
		for(int i=0;i<counter.getNumberOfShards();i++){
			Shard shard=ofy().load().type(Shard.class).id(counter.getName()+"_"+i).now();
			if(shard!=null){
				sum+=shard.getValue();
			}
		}
		return sum;
	}
	
	
	
	
}
