package eu.revevol.shardedcounter.entity;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/*
 * Class that describes a transaction key.
 */

@Entity
public class TxnKey {

	//variables
	@Id String txnKey;
	long timestamp;
	
	public TxnKey(String txnKey){
		this.txnKey=txnKey;
		this.timestamp=new java.util.Date().getTime();
	}
	
	/*
	 * getters and setters
	 */
	
	public String getTnxKey() {
		return txnKey;
	}
	public void setTnxKey(String txnKey) {
		this.txnKey = txnKey;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	
	
}
