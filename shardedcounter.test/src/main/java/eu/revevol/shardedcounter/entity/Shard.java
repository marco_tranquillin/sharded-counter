package eu.revevol.shardedcounter.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/*
 * Class that describes a shard.
 * It contains a single value
 */

@Entity
public class Shard {

	//variables
	@Id String name;
	int value;
	
	
	/*
	 * Increment the value of the shard
	 * @return the incremented valued of the shard
	 */
	public int increment(){
		return (++value);
	}
	
	/*
	 * Decrement the value of the shard
	 * @return decremented value of the shard
	 */
	public int decrement(){
		return (--value);
	}
	
	/*
	 * getters and setters
	 */
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	
	
}
