package eu.revevol.shardedcounter.entity;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/*
 * Shards number container.
 */

@Entity
public class Counter {

	//variables
	@Id String name;
	int numberOfShards;
	
	
	/*
	 * Increment the number of shards
	 * @return the incremented value of the numberOfShards
	 */
	public int increment(){
		return (++numberOfShards);
	}
	
	/*
	 * Decrement the number of shards
	 * @return the decremented value of the numberOfShards
	 */
	public int decrement(){
		return (--numberOfShards);
	}
	
	/*
	 * getters and setters
	 */
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getNumberOfShards() {
		return numberOfShards;
	}
	
	public void setNumberOfShards(int numberOfShards) {
		this.numberOfShards = numberOfShards;
	}
}
