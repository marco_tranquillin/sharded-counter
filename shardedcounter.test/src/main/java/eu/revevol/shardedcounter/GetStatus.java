package eu.revevol.shardedcounter;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import eu.revevol.shardedcounter.servlet.RESTResponse;

public class GetStatus extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4796720862492766569L;
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doPost(req,resp);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		Gson gson=new Gson();
		//get counter name
		String counterName=req.getParameter("counterName");
		if(counterName==null){
			RESTResponse restResponse=new RESTResponse();
				restResponse.setCode(400);
				restResponse.setMessage("Counter name missing");
			resp.setContentType("application/json");
			resp.getWriter().println(gson.toJson(restResponse));
			return;
		}
		
		//load ShardedCounter
		ShardedCounter sc=new ShardedCounter();
		if(!sc.load(counterName)){
			RESTResponse restResponse=new RESTResponse();
				restResponse.setCode(400);
				restResponse.setMessage("Counter has not been registered");
				resp.setContentType("application/json");
				resp.getWriter().println(gson.toJson(restResponse));
			return;
		}
		//get status
		RESTResponse restResponse=new RESTResponse();
			restResponse.setCode(200);
			restResponse.setMessage(""+sc.getCounterValue());
			resp.setContentType("application/json");
			resp.getWriter().println(gson.toJson(restResponse));
		return;
		
	}

}
