package eu.revevol.shardedcounter.task;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.github.rholder.retry.WaitStrategies;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.TableDataInsertAllRequest;
import com.google.api.services.bigquery.model.TableDataInsertAllRequest.Rows;
import com.google.api.services.bigquery.model.TableDataInsertAllResponse;
import com.google.api.services.bigquery.model.TableRow;
import com.google.common.base.Predicates;
import eu.revevol.shardedcounter.ShardedCounter;

public class UpdateSc extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3675790370559898968L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doPost(req,resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		//get counter name
		String counterName=req.getParameter("counterName");
		
		//get access to the ShardedCounter
		ShardedCounter sc=new ShardedCounter();
			sc.load(counterName);
		
		//increment the sharded counter
		String txnID= UUID.randomUUID().toString();
		long txnStart=new java.util.Date().getTime();
		sc.updateShard(txnID, true);
		long txnEnd=new java.util.Date().getTime();
		
		//write record into BigQuery
		insertRecordBigQuery(txnID,counterName,txnStart,txnEnd);
		
		return;
	}
	
	/**
	 * Insert a record into BigQuery
	 * @param txnID transaction ID
	 * @param counterName the name of the updated counter
	 * @param txnStart timestamp when the transaction starts
	 * @param txnEnd timestamp when the tansaction ends
	 */
	private void insertRecordBigQuery(final String txnID, final String counterName, final long txnStart, final long txnEnd){
		
		// define HTTP_TRANSPORT and JSON_FACTORY
		final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
		final JsonFactory JSON_FACTORY = new JacksonFactory();
		
		/**
		 * OAuth 2.0 Data
		 */
		 final String		SERVICE_ACCOUNT_KEY_PATH		= "WEB-INF/key/ServiceAccountPK.p12";
		 final String		SERVICE_ACCOUNT_EMAIL_ADDRESS	= "941432327666-omg587jsf1elopjbrnd7po16a60ptnlg@developer.gserviceaccount.com";
		 final String[]		OAUTH_SCOPES					= { "https://www.googleapis.com/auth/bigquery" };
		
		/**
		 * Google BigQuery
		 */
		 final String		PROJECT_ID			= "task-mgmt-sharded-counter";
		 final String	    BQ_DATASET_ID		= "sharded_counter_test";
		 final String	    BQ_TABLE_ID			= "TRANSACTIONS";

		 //define task to be executed
		 Callable<Boolean> bqTask = new Callable<Boolean>() {
			    public Boolean call() throws Exception {
			    	// load SERVICE ACCOUNT private key
					InputStream privateKeyStream = null;
					privateKeyStream = new FileInputStream(getServletContext().getRealPath(SERVICE_ACCOUNT_KEY_PATH));
					KeyStore ks = KeyStore.getInstance("PKCS12");
					ks.load(privateKeyStream, "notasecret".toCharArray());
					PrivateKey myOwnKey = (PrivateKey) ks.getKey("privatekey", "notasecret".toCharArray());
					
					GoogleCredential credential = new GoogleCredential.Builder()
						.setTransport(HTTP_TRANSPORT)
						.setJsonFactory(JSON_FACTORY)
						.setServiceAccountId(SERVICE_ACCOUNT_EMAIL_ADDRESS)
						.setServiceAccountScopes(Arrays.asList(OAUTH_SCOPES))
						.setServiceAccountPrivateKey(myOwnKey)
						.build();
					credential.refreshToken();
				
					Bigquery bq = new Bigquery.Builder(HTTP_TRANSPORT, JSON_FACTORY,credential)
					.setApplicationName("ShardedCounter")
					.setHttpRequestInitializer(credential).build();
					
					//insert row into BigQuery
					TableRow row = new TableRow();
						row.set("ID", txnID);
						row.set("COUNTER_NAME",counterName );
						row.set("START", txnStart);
						row.set("START_TIMESTAMP", new DateTime(new Date(txnStart)));
						row.set("END", txnEnd);
						row.set("END_TIMESTAMP", new DateTime(new Date(txnEnd)));
					
					TableDataInsertAllRequest.Rows rows = new TableDataInsertAllRequest.Rows();
						rows.setInsertId(UUID.randomUUID().toString());
						rows.setJson(row);
					
					List<Rows> rowList =new ArrayList();
						rowList.add(rows);
					
					TableDataInsertAllRequest content = new TableDataInsertAllRequest().setRows(rowList);
					TableDataInsertAllResponse bqRes =
					    bq.tabledata().insertAll(
					        PROJECT_ID, 
					        BQ_DATASET_ID, 
					        BQ_TABLE_ID, 
					        content)
					    .execute();
					
			        return true; //return task
			    }
		};
		
		//define Retry strategy for the task
		Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
		        .retryIfResult(Predicates.<Boolean>isNull())
		        .retryIfExceptionOfType(IOException.class)
		        .retryIfRuntimeException()
		        .withWaitStrategy(WaitStrategies.exponentialWait(2, 32,TimeUnit.SECONDS))
		        .withStopStrategy(StopStrategies.stopAfterAttempt(5))
		        .build();

		
		//execute task
		try {
		    retryer.call(bqTask);
		} catch (RetryException e) {
		    e.printStackTrace();
		} catch (ExecutionException e) {
		    e.printStackTrace();
		}
	}

}
