package eu.revevol.shardedcounter.task;

import java.io.IOException;
import java.util.Random;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

public class LaunchUpdateSc extends HttpServlet {
	
	final long REMAINING_TIME_LIMIT=60000; //one minute

	/**
	 * 
	 */
	private static final long serialVersionUID = 3675790370559898968L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doPost(req,resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		long numberOfTasks=Long.parseLong(req.getParameter("numberOfTasks"));
		String counterName=req.getParameter("counterName");
		final long MAX_NUM_TASKS=1000;
		
		//get Random value between 0 and 10 (we have 10 queues)
		Random rnd=new Random();
		int max=10;
		
		//execute tasks
		for(long i=0; i<numberOfTasks && i<MAX_NUM_TASKS;i++){
			int nextQueue=rnd.nextInt(max);
			Queue queue = QueueFactory.getQueue("queue-" + nextQueue);
	        //define task options
	        TaskOptions to = TaskOptions.Builder
	          .withUrl("/task/updateSc")
	          .param("counterName", counterName);
	        queue.add(to);
	        //decrement numberOfTasks
		    numberOfTasks--;
		}
		
		//if you have more than MAX_NUM_TASKS launch parallel execution
		if(numberOfTasks>0){
			//enqueue remaining tasks
			Queue queue = QueueFactory.getDefaultQueue();
			//define task options
		    TaskOptions to = TaskOptions.Builder
		    		.withUrl("/task/updateSc/launch")
		    		.param("numberOfTasks", ""+numberOfTasks)
		    		.param("counterName", counterName);
		    queue.add(to);
		}
		return;
		
	}

}
