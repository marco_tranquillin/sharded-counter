package eu.revevol.shardedcounter;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;

import eu.revevol.shardedcounter.servlet.RESTResponse;


public class StressTest extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4796720862492766569L;
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doPost(req,resp);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		Gson gson=new Gson();
		
		//get counter name
		String counterName=req.getParameter("counterName");
		if(counterName==null){
			RESTResponse restResponse=new RESTResponse();
				restResponse.setCode(400);
				restResponse.setMessage("Counter name missing");
			resp.setContentType("application/json");
			resp.getWriter().println(gson.toJson(restResponse));
			return;
		}
				
		//get number of tasks to be launched
		if(req.getParameter("numberOfTasks")==null){
			RESTResponse restResponse=new RESTResponse();
				restResponse.setCode(400);
				restResponse.setMessage("Number of task missing");
				resp.setContentType("application/json");
				resp.getWriter().println(gson.toJson(restResponse));
			return;
		}
		long numberOfTasks=Long.parseLong(req.getParameter("numberOfTasks"));
			
		Queue queue = QueueFactory.getDefaultQueue();
	    //define task options
	    TaskOptions to = TaskOptions.Builder
	    		.withUrl("/task/updateSc/launch")
	    		.param("numberOfTasks", ""+numberOfTasks)
	    		.param("counterName", counterName);
	    
	    queue.add(to);
	}

}
