package eu.revevol.shardedcounter.servlet;


/*
 * Shards number container.
 */

public class RESTResponse {

	//variables
	int code;
	String message;
	
	/**
	 * Default constructor
	 */
	public RESTResponse(){
	}
	
	/**
	 * Constructor
	 * @param code html code
	 * @param message message to be returned
	 */
	public RESTResponse(int code,String message){
		this.code=code;
		this.message=message;
	}


	public int getCode() {
		return code;
	}


	public void setCode(int code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}
}
